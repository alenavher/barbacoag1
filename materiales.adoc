== Lista de materiales

// Materiales necesarios por categoría, ordenados alfabéticamente

=== Cocina y comida

* Cuchillos
* Platos
* Tenedores
* Vasos

=== Mobiliario

* Mesas
* Sillas

=== Diversión

* Altavoz
* Colchoneta
* Pelota
* Radiocassette
